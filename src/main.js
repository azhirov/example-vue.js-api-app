import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/ru-RU'
import App from './App'
import Axios from 'axios'; // библиотека для отправки HTTP-запросов к API

Vue.use(ElementUI, { locale })

Vue.prototype.$axios = Axios.create({
  baseURL: 'https://api.backendless.com/F2ABB7C7-75A8-A946-FF8E-063D2DB0B400/674ADAB3-AB89-75BB-FF91-FF8B4E102100',
  timeout: 1000,
});

Vue.config.productionTip = false

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
